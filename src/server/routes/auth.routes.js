/*jshint esversion: 6*/
var router = require('express').Router();
var authDao = require('./../dao/auth.dao');
var failMessage = require('./../services/failMessage');
var reCaptcha = require('././../utils/captcha');
var middlewareValidator = require('./../middlewares/validator');
// var session = require('express-session');
module.exports = function() {

    router.post('/signin/:state', middlewareValidator.validator(), signin);
    router.post('/signup', middlewareValidator.validator(), signup);
    router.post('/sendEmail', sendEmail);
    router.post('/resetPassword', resetPassword);

    function signin(req, res, next) {
        var request = {
            email: req.body.email,
            password: req.body.password,
            state: req.params.state
        };
        if (!request.email || !request.password) {
            res.status(403).send(failMessage.user.login.input).end();
        }
        var ip = req.headers['x-forwarded-for'] ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress ||
            req.connection.socket.remoteAddress;
        authDao.signin(request, ip)
            .then((response) => {
                res.status(200).send(response).end();
            })
            .catch((err) => {
                next(err);
            });
    }

    function signup(req, res, next) {
        var request = {
            email: req.body.email,
            password: req.body.password,
            username: req.body.username,
            birthday: req.body.birthday,
            gender: req.body.gender,
            phone: req.body.phone,
            role: 'user',
        };
        if (request.email === '' || !request.password || !request.username) {
            res.status(403).send(failMessage.user.register.input).end();
        }
        var ip = req.headers['x-forwarded-for'] ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress ||
            req.connection.socket.remoteAddress;
        reCaptcha.checkCaptcha(req.body.reCaptcha, ip)
            .then((val) => {
                if (val.success) {
                    authDao.signup(request)
                        .then((response) => {
                            res.status(200).send(response).end();
                        })
                        .catch((err) => {
                            next(err);
                        });
                }
            }).catch((err) => {
                next(err);
            });



    }




    function sendEmail(req, res, next) {
        var request = {
            email: req.body.email
        };
        if (!request.email) {
            return Promise.reject({
                statusCode: 403,
                message: failMessage.user.login.input
            });
        }

        authDao.sendEmail(request)
            .then((response) => {
                res.status(200).send(response).end();
            })
            .catch((err) => {
                next(err);
            });
    }

    function resetPassword(req, res, next) {
        var request = {
            newPassword: req.body.newPassword,
            token: req.body.token
        };
        if (!request.newPassword || !request.token) {
            res.status(403).send(failMessage.user.login.input).end();
        }

        authDao.resetPassword(request)
            .then((response) => {
                res.status(200).send(response).end();
            })
            .catch((err) => {
                console.log(err);
                next(err);
            });
    }

    // function signin(req, res, next) {
    //     var request = {
    //         email: req.body.email,
    //         password: req.body.password
    //     };
    //     authDao.signin(request, function (err, response) {
    //         if (err) { next(err); }
    //         else {
    //             res.status(200).send(response).end();
    //         }
    //     });
    // }

    // function signup(req, res, next) {
    //     var request = {
    //         email: req.body.email,
    //         password: req.body.password,
    //         username: req.body.username,
    //         firstname: req.body.firstname,
    //         lastname: req.body.lastname,
    //         image: req.body.image,
    //         birthday: req.body.birthday,
    //         gender: req.body.gender,
    //         position: req.body.position,
    //         address: req.body.address,
    //         phone: req.body.phone,
    //         roles: 'user'
    //     };

    //     authDao.signup(request, function (err, response) {
    //         if (err) {
    //             next(err);
    //         } else {
    //             res.status(200).send(response).end();
    //         }
    //     });
    // }

    // function sendEmail(req, res, next) {
    //     var request = {
    //         email: req.body.email
    //     };
    //     authDao.sendEmail(request, function (err, response) {
    //         if (err) {
    //             next(err);
    //         } else {
    //             res.status(200).send(response).end();
    //         }
    //     });
    // }

    // function resetPassword(req, res, next) {
    //     var request = {
    //         newPassword: req.body.newPassword,
    //         token: req.body.token
    //     };
    //     authDao.resetPassword(request, function (err, response) {
    //         if (err) {
    //             next(err);
    //         } else {
    //             res.status(200).send(response).end();
    //         }
    //     });
    // }

    return router;
};