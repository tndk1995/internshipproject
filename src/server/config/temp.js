var express = require('express');
var app = express();
var insecureApp = express();
var insecurePort = 8001;
var config = require('./config/config');
var fs = require('fs');
var path = require('path');
var rootPath = __dirname;
var options = {
    key: fs.readFileSync(path.join(rootPath, 'server.key')),
    cert: fs.readFileSync(path.join(rootPath, 'server.crt'))
};
app.use(bodyParser.json());
app.use(bodyParser.json({
    type: ['json', 'application/csp-report']
}));
app.use(bodyParser.urlencoded({
    extended: false
}));

redirectRequests(insecureApp);
responseHeaderConfig(app);
insecureApp.listen(insecurePort, function(err) {
    if (err) {
        console.log(err);
    } else {
        console.log('Express server listening on port ' + insecurePort);
        console.log('env = ' + app.get('env') +
            '\n__dirname = ' + __dirname +
            '\nprocess.cwd = ' + process.cwd());
    }
});
https.createServer(options, app).listen(443);