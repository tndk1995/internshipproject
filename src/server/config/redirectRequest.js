/*jshint esversion: 6*/
'use strict';

function httpsRedirectConfig(app) {
    app.all('*', ((req, res, next) => {
        res.redirect(307, 'https://localhost' + req.url);
        next();
    }));
}
module.exports = httpsRedirectConfig;