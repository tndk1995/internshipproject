var user = {
    login: 'LOGIN_SUCESS',
    register: 'REGISTER_SUCCESS',
    signup: 'CREATE_SUCCESS',
    changePassword: 'CHANGE_SUCCESS',
    sendEmail: 'SEND_SUCCESS',
    captcha: 'CAPTCHA_SUCCESS',
    update: 'UPDATE_SUCCESS',
    remove: 'REMOVE_SUCCESS'
};


module.exports = {
    user: user
};