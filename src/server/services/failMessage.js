var user = {
    login: {
        input: 'ERROR_INPUT',
        systemErr: 'SYSTEM_ERROR',
        notFound: 'USER_NOT_FOUND',
        inCorrect: 'PASSWORD_INCORRECT',
        over5Time: 'OVER_TIME',
        inProgress: 'IN_PROGRESS'
    },
    signup: {
        input: 'ERROR_INPUT',
        systemErr: 'SYSTEM_ERROR',
        duplicateUser: 'DUPLICATE_USER'
    },
    register: {
        input: 'ERROR_INPUT'
    },
    changePassword: {
        systemErr: 'SYSTEM_ERROR',
        passwordOldNotCorrect: 'PASSWORD_OLD_NOT_CORRECT',
        input: 'ERROR_INPUT'
    },
    captcha: {
        noCaptcha: "NO_CAPTCHA",
        failCaptcha: "FAIL_CAPTCHA"
    },
    jwt: {
        unauthorized: "UN_AUTHORIZED",
        noToken: "NO_TOKEN",
        expired: "TOKEN_EXPIRED"
    }
};


module.exports = {
    user: user
};