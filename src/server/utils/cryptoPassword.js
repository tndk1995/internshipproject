/*jshint esversion: 6*/
'use strict';
var crypto = require('crypto');
const salt = 'secret';

module.exports = {
    cryptoPassword: cryptoPassword,
    verifyPassword: verifyPassword
};

function cryptoPassword(password) {
    var salt = genRandomString(16);
    var hash = crypto.pbkdf2Sync(password, salt, 100000, 512, 'sha512');

    return {
        salt: salt,
        hash: hash.toString('base64')
    };
}

function verifyPassword(savedHash, savedSalt, passwordAttempt) {
    return savedHash === crypto.pbkdf2Sync(passwordAttempt, savedSalt, 100000, 512, 'sha512').toString('base64');
}
/**
 * generates random string of characters i.e salt
 * @function
 * @param {number} length - Length of the random string.
 */
function genRandomString(length) {
    return crypto.randomBytes(64)
        .toString('base64')
        .slice(0, length)
        .replace(/\+/g, '0')
        .replace(/\//g, '0');
}