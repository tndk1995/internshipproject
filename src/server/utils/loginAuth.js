var loginAuth = require('./../model/login.model');
var moment = require('moment');
module.exports = {
    canAuthenticate: canAuthenticate,
    failedLoginAttempt: failedLoginAttempt,
    successfulLoginAttempt: successfulLoginAttempt,
    inProgress: inProgress
}

function canAuthenticate(key) {
    return new Promise(function(resolve, reject) {
        return loginAuth.findOne({
            indentityKey: key
        }).then((checkLogin) => {
            if (!checkLogin || checkLogin.failedAttempts < 5) {
                return resolve(true);
            }
            var timeOut = (moment() - moment(checkLogin.timeOut).add(1, 'm'));
            if (timeOut >= 0) {
                checkLogin.remove();
                return resolve(true);
            }
            return resolve(false);
        }).catch((err) => {
            return reject(err);
        });

    });

}

function failedLoginAttempt(key) {
    return new Promise(function(resolve, reject) {
        var query = { indentityKey: key };
        var update = { $inc: { failedAttempts: 1 }, timeOut: new Date(), inProgress: false };
        var options = { upsert: true, setDefaultsOnInsert: true };
        return loginAuth.findOneAndUpdate(query, update, options).exec()
            .then((res) => {
                return resolve();
            })
            .catch((err) => {
                return reject(err);
            });
    });
}

function successfulLoginAttempt(key) {
    return new Promise(function(resolve, reject) {
        return loginAuth.findOneAndRemove({ indentityKey: key })
            .then((res) => {
                return resolve();
            })
            .catch((err) => {
                return reject(err);
            });
    });
}

function inProgress(key) {
    return new Promise(function(resolve, reject) {
        var query = { indentityKey: key };
        return loginAuth.findOne(query)
            .then((res) => {

                if (res && res.inProgress && res.failedAttempts < 5) {
                    return resolve(true);
                } else if (res && !res.inProgress) {
                    var update = { inProgress: true };
                    var options = { upsert: true, setDefaultsOnInsert: true };
                    return loginAuth.findOneAndUpdate(query, update, options).exec()
                        .then((res) => {
                            return resolve(false);
                        })
                        .catch((err) => {
                            return reject(err);
                        });
                }
                return resolve(false);

            });

    });
}