'use strict';
var jwt = require('jsonwebtoken');
var config = require('../config/config');
module.exports = {
    signToken: signToken
};

function signToken(information, state) {
    if (state === "0") {
        console.log(state);
        var token = jwt.sign(information, config.certKey, { algorithm: 'RS256', expiresIn: '1m' });
    } else {
        var token = jwt.sign(information, config.certKey, { algorithm: 'RS256', expiresIn: '7d' });
    }
    return token;
}