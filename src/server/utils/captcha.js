var request = require('request');
var successMessage = require('./../services/successMessage');
var failMessage = require('./../services/failMessage');

function checkCaptcha(response, ip) {
    return new Promise(function(resolve, reject) {
        if (response === undefined || response === '' || response === null) {
            console.log(2);
            return reject({
                message: failMessage.user.captcha.noCaptcha
            });
        }
        // var ip = "127.0.0.1";
        var secretKey = "6LdSWSgUAAAAAGO5q-c-V67wg9U9lZq4bm6nWfD2";
        var verificationUrl = "https://www.google.com/recaptcha/api/siteverify?secret=" + secretKey + "&response=" + response + "&remoteip=" + ip;

        request(verificationUrl, function(error, response, body) {
            if (error) {
                return reject({
                    message: failMessage.user.captcha.failCaptcha
                });
            }
            try {
                resolve(JSON.parse(body));
            } catch (e) {
                reject(e);
            }
        });
    });
}
module.exports = {
    checkCaptcha: checkCaptcha
}