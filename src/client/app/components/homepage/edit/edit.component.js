angular.module('app.homepage')
    .component('edit', {
        bindToController: true,
        templateUrl: 'app/components/homepage/edit/edit.html',
        controller: editController,
        controllerAs: 'vm',
        bindings: {
            user: '=',
            namePage: '=',
            role: '='
        }
    });

editController.$inject = ['homeService', '$state'];

function editController(homeService, $state) {
    var vm = this;
    vm.changePass = false;
    vm.gender = vm.user.gender;
    vm.birthdayConvert = moment(vm.user.birthday).format('DD-MM-YYYY');
    vm.edit = edit;
    vm.cancel = cancel;

    function edit() {
        var update = {
            _id: vm.user._id,
            username: vm.username,
            birthday: new Date(vm.birthday),
            phone: vm.phone,
            gender: vm.gender
        };
        if (vm.changePass) {
            update.oldPass = vm.oldPassword;
            update.newPass = vm.password;
        }
        return homeService.updateUser(update).then(
            function(res) {
                toastr.success(res);
                $state.reload();
            },
            function(err) {
                toastr.error(err);
            }
        );
    }

    function cancel() {
        $state.reload();
    }

}