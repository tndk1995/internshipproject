angular.module('app.homepage')
    .controller('homepageController', ['$state', 'homeService', 'authService', homepageController]);

function homepageController($state, homeService, authService) {
    var vm = this;
    vm.namePage = 0;
    vm.logout = logout;


    if (vm.namePage == 0) {
        homeService.getUser().then(
            function(res) {
                vm.user = res.data.user;
                vm.userEdit = vm.user;
                vm.namePage = 1;
            },
            function(err) {
                toastr.error(err);
            });

    }

    function logout() {
        toastr.success(authService.logout());
        $state.go('auth.login');
    }


}