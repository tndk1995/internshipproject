angular.module('app.homepage')
    .component('profile', {
        bindToController: true,
        templateUrl: 'app/components/homepage/profile/profile.html',
        controller: profileController,
        controllerAs: 'vm',
        bindings: {
            user: '='
        }
    });

function profileController() {
    var vm = this;
    vm.birthday = moment(vm.user.birthday).format('DD-MM-YYYY');
}