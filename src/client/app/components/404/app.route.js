(function() {
    'use strict';

    angular
        .module('app')
        .run(appRun)
        .config(authConfig);

    function authConfig($stateProvider, $httpProvider) {
        $stateProvider
            .state('root', {
                url: '/',
            });
        $httpProvider.interceptors.push('tokenInterceptor');

    }

    appRun.$inject = ['$state', '$rootScope', 'authService', 'routerHelper'];

    function appRun($state, $rootScope, authService, routerHelper) {

        var otherwise = '/404';

        routerHelper.configureStates(getStates(), otherwise);

        $rootScope.$on('$stateChangeStart', function(event, toState, fromState) {
            if (toState.url === '/login' || toState.url === '/register') {
                if (authService.login(null, 2)) {
                    event.preventDefault();
                    $state.go('homepage');
                }
            } else if (toState.url === '/homepage') {
                if (!authService.login(null, 2)) {
                    event.preventDefault();
                    $state.go('auth.login');
                }
            } else if (toState.url === '/') {
                event.preventDefault();
                $state.go('auth.login');
            }

        });
    }

    function getStates() {
        return [{
            state: '404',
            config: {
                url: '/404',
                templateUrl: 'app/components/404/404.html',
                title: '404'
            }
        }];
    }
})();