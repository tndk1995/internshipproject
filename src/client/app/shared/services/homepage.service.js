 angular.module('services.hompage', ['services.errorTranslator', 'services.auth'])
     .factory('homeService', ['$q', '$http', 'errTransService', 'authService', homeService]);

 function homeService($q, $http, errTransService, authService) {
     return {
         getUser: getUser,
         updateUser: updateUser,
         getAllUser: getAllUser,
         deleteUser: deleteUser
     }

     function getUser() {
         var deferred = $q.defer();
         $http.get('api/users')
             .then(function(res) {
                 deferred.resolve(res);
             }, function(err) {
                 deferred.reject(errTransService[err.data.message]);
             });

         return deferred.promise;
     }

     function getAllUser(pageIndex) {
         var deferred = $q.defer();

         $http.get('api/users/' + pageIndex)
             .then(function(res) {
                 deferred.resolve(res);
             }, function(err) {
                 deferred.reject(errTransService[err.data.message]);
             });

         return deferred.promise;
     }

     function updateUser(newUser) {
         var deferred = $q.defer();
         if (newUser) {
             $http.put('api/users', newUser)
                 .then(function(res) {
                     deferred.resolve('Update successful');
                 }, function(err) {
                     deferred.reject(errTransService[err.data.message]);
                 });
         }
         return deferred.promise;
     }

     function deleteUser(id) {
         var deferred = $q.defer();

         $http.delete('api/users/' + id)
             .then(function(res) {
                 deferred.resolve('Delete successful');
             }, function(err) {
                 deferred.reject(errTransService[err.data.message]);
             });

         return deferred.promise;
     }

 }